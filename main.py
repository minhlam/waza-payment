from flask_script import Manager
from flask import Flask
from payment.payment_api import blue
from flask_cors import CORS

def create_app(config='dev'):
    app = Flask(__name__)
    app.config['SECRET_KEY'] = '123456789'
    app.register_blueprint(blue)
    CORS(app)
    return app

app = create_app()
# manager = Manager(create_app())
# manager.add_option('-c', '--config', dest='config', required=False)

if __name__ == '__main__':
    app.run()
