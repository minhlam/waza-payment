from pymongo import MongoClient


class ConnectionString(object):
    # client = MongoClient("mongodb+srv://nghia:nghia@cluster0-lupss.gcp.mongodb.net/test?retryWrites=true&w=majority")
    # db_name = client['test']

    client = MongoClient("mongodb+srv://payment:wazapayment@cluster0-sdlir.mongodb.net/test?retryWrites=true&w=majority")
    db_name = client['waza_payment']

    @staticmethod
    def init_db(collection):
        ConnectionString.db = ConnectionString.db_name[collection]
        return ConnectionString.db
