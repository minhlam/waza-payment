import math
from database_connection.connection import ConnectionString
from flask import Blueprint, request, jsonify
from flask_cors import cross_origin
import datetime
import json
import time

db_wallet = ConnectionString.init_db('waza-wallet')
db_cash = ConnectionString.init_db('waza-cash')
db_param = ConnectionString.init_db('payment-param')
db_log = ConnectionString.init_db('payment-log')

db_report = ConnectionString.init_db('report-log')

blue = Blueprint('admin', __name__)


def UpdateParam(param_id, data):
    if not data:
        return {"response": "NO DATA"}

    if not param_id:
        return {"response": "MISSING PARAM_ID"}

    query = {'param_id': param_id}
    value = {"$set": {'value': data}}
    db_param.update_one(query, value)
    return {"reponse": "SUCCESS"}


def get_parameter(param_id=None):
    if param_id:
        param = db_param.find_one({"param_id": param_id}, {"_id": 0})
        if param:
            return param
    else:
        params = db_param.find({}, {"_id": 0})
        list_param = []
        if params:
            for param in params:
                list_param.append(param)
        return list_param

# Get param
@blue.route('/get_param/<param_id>', methods=['GET'])
@cross_origin()
def get_param(param_id):
    if not param_id:
        return {'response': get_parameter()}

    param = get_parameter(param_id)
    if param:
        return {'response': param}
    return {'response': 'NO DATA RECEIVE'}
# Get all param
@blue.route('/get_all_param', methods=['GET'])
@cross_origin()
def get_all_param():
    param = get_parameter()
    return {'response': param}

# Them param
@blue.route('/param', methods=['POST'])
@cross_origin()
def add_param():
    data = request.get_json()
    if not data:
        return {"response": "NO DATA"}

    param_id = data.get('param_id')
    value = data.get('value')

    if not param_id or not value:
        return {"response": "MISSING DATA"}

    result = db_param.find_one({"param_id": param_id})
    if result:
        return {"response": "DUPLICATE"}
    else:
        param_raw = {
            'param_id': param_id,
            'value': value
        }
        db_param.insert_one(param_raw)
        return {"response": "SUCCESS"}

# Update Param
@blue.route('/update_param', methods=['POST'])
@cross_origin()
def update_param():
    data = request.get_json()
    value = data.get('value')
    if not data:
        return {"response": "NO DATA"}

    param_id = data.get('param_id')

    if not param_id:
        return {"response": "MISSING PARAM_ID"}

    param_found = db_param.find_one({"param_id": param_id})
    if not param_found:
        return {"response": "NO PARAM ID IS %s" % param_id}

    UpdateParam(param_id, value)

    return {"response": "SUCCESS"}


def get_wallet(user_id=None):
    if user_id:
        wallet = db_wallet.find_one({"user_id": user_id}, {"_id": 0})
        if wallet:
            return wallet
    else:
        wallets = db_wallet.find({}, {"_id": 0})
        list_wallet = []
        if wallets:
            for wallet in wallets:
                list_wallet.append(wallet)
        return list_wallet


def get_cash(user_id=None):
    if user_id:
        cash = db_cash.find_one({"user_id": user_id}, {"_id": 0})
        if cash:
            return cash
    else:
        cashs = db_cash.find({}, {"_id": 0})
        list_cash = []
        if cashs:
            for cash in cashs:
                list_cash.append(cash)
        return list_cash


def AddWallet(user_id, value):
    result = db_wallet.find_one({"user_id": user_id})
    if result:
        return {"response": "DUPLICATE"}
    else:
        wallet_raw = {
            'user_id': user_id,
            'value': value
        }
        db_wallet.insert_one(wallet_raw)


def UpdateWallet(user_id, data):
    if not data:
        return {"response": "NO DATA"}

    if not user_id:
        return {"response": "MISSING USER_ID"}

    query = {'user_id': user_id}
    value = {"$set": {'value': data}}
    db_wallet.update_one(query, value)
    return {"reponse": "SUCCESS"}


def AddCash(user_id, value):
    result = db_cash.find_one({"user_id": user_id})
    if result:
        return {"response": "DUPLICATE"}
    else:
        cash_raw = {
            'user_id': user_id,
            'value': value
        }
        db_cash.insert_one(cash_raw)


def UpdateCash(user_id, data):
    if not data:
        return {"response": "NO DATA"}

    if not user_id:
        return {"response": "MISSING USER_ID"}

    query = {'user_id': user_id}
    value = {"$set": {'value': data}}
    db_cash.update_one(query, value)
    return {"reponse": "SUCCESS"}

# Lay list cash
@blue.route('/list-cash', methods=['GET'])
@cross_origin()
def list_cash():
    cashs = db_cash.find({}, {"_id": 0})
    data = []
    if cashs:
        for cash in cashs:
            data.append(cash)
        return jsonify(data)
    return {'response': 'NO DATA'}

# Lay wallet theo user
@blue.route('/wallet/<user_id>', methods=['GET'])
@cross_origin()
def user_wallet(user_id):
    if user_id:
        wallet = db_wallet.find_one({"user_id": str(user_id)}, {"_id": 0})
        if wallet:
            return jsonify(wallet)
    return {'response': 'NO DATA RECEIVE'}
    
# Lay cash theo user_id
@blue.route('/cash/<user_id>', methods=['GET'])
@cross_origin()
def user_cash(user_id):
    if user_id:
        cash = db_cash.find_one({"user_id": str(user_id)}, {"_id": 0})
        if cash:
            return jsonify(cash)
    return {'response': 'NO DATA RECEIVE'}

# Them tien vao cash
@blue.route('/cash', methods=['POST'])
@cross_origin()
def post_cash():
    data = request.get_json()
    if not data:
        return {"response": "NO DATA"}

    user_id = data.get('user_id')
    value = data.get('value')

    if not user_id or not value:
        return {"response": "MISSING DATA"}
    if value <= 0:
        return {"response": "VALUE INVALID"}
    result = db_cash.find_one({"user_id": user_id})
    if not result:
        return {"response": "CANNOT FIND USER CASH"}

    query = {'user_id': user_id}
    value = {"$set": {'value': result['value'] +  value}}
    db_cash.update_one(query,value)
    return {"response": "SUCCESS"}

# Lay list wallet
@blue.route('/list-wallet', methods=['GET'])
@cross_origin()
def list_wallet():
    wallets = db_wallet.find({}, {"_id": 0})
    data = []
    if wallets:
        for wallet in wallets:
            data.append(wallet)
        return jsonify(data)
    return {'response': 'NO DATA'}




# Thêm tài khoản wallet và cash cho user
@blue.route('/create_wallet', methods=['POST'])
@cross_origin()
def add_wallet():
    data = request.get_json()
    if not data:
        return {"response": "NO DATA"}
    user_id = data.get('user_id')
    if not user_id:
        return {"response": "MISSING DATA"}
    user_id = str(user_id)
    resultWallet = db_wallet.find_one({"user_id": user_id})
    resultCash = db_cash.find_one({"user_id": user_id})

    if resultWallet or resultCash:
        return {"response": "DUPLICATE"}
    else:
        wallet_raw = {
            'user_id': user_id,
            'value': 0
        }
        cash_raw = {
            'user_id': user_id,
            'value': 0
        }
        db_wallet.insert_one(wallet_raw)
        db_cash.insert_one(cash_raw)
        return {"response": "SUCCESS"}


# Chuyển tiền từ cash sang wallet
@blue.route('/transfer_from_cash', methods=['POST'])
@cross_origin()
def transfer_from_cash1():
    data = request.get_json()
    if not data:
        return {"response": "NO DATA"}

    user_id = data.get('user_id')
    cash = data.get('value')
    if not user_id or not cash:
        return {"response": "MISSING DATA"}
    if cash <= 0:
        return {"response": "CASH INVALID"}
    userWallet = get_wallet(user_id)
    userCash = get_cash(user_id)
    if not userWallet or not userCash:
        return {"response": "USER DOESN'T EXSIST"}
    if userCash['value'] < cash:
        return {"response": "USER CASH DONT HAVE ENOUGH VALUE TO TRANSFER"}
    userWallet['value'] = userWallet['value'] + cash
    userCash['value'] = userCash['value'] - cash
    UpdateCash(userCash['user_id'], userCash['value'])
    UpdateWallet(userWallet['user_id'], userWallet['value'])
    return {"response": "SUCCESS"}


# Chuyển tiền từ wallet sang cash
@blue.route('/transfer_from_wallet', methods=['POST'])
@cross_origin()
def transfer_from_wallet():
    data = request.get_json()
    if not data:
        return {"response": "NO DATA"}
    
    user_id = data.get('user_id')
    wallet = data.get('value')
    if not user_id or not wallet:
        return {"response": "MISSING DATA"}

    userWallet = get_wallet(user_id)
    userCash = get_cash(user_id)
    if not userWallet or not userCash:
        return {"response": "USER DOESN'T EXSIST"}
    if wallet <= 0:
        return {"response": "VALUE INVALID"}
    if userWallet['value'] < wallet:
        return {"response": "USER WALLET DONT HAVE ENOUGH VALUE TO TRANSFER"}
    userWallet['value'] = userWallet['value'] - wallet
    userCash['value'] = userCash['value'] + wallet
    UpdateCash(userCash['user_id'], userCash['value'])
    UpdateWallet(userWallet['user_id'], userWallet['value'])

    return {"response": "SUCCESS"}


# Kiểm tra ví tiền của driver có hợp lệ
@blue.route('/check_driver_wallet', methods=['POST'])
@cross_origin()
def check_driver_wallet():
    data = request.get_json()
    user_id = data.get('user_id')
    service = data.get('service')
    if user_id and service:
        userFind = get_wallet(user_id)
        param = None
        if not userFind:
            return {'response': 'CANNOT FIND USER'}
        if service == "SHOPPING":
            param = get_parameter("shopping_deposit")
        elif service == "FOOD":
            param = get_parameter("food_deposit")
        elif service == "BIKE":
            param = get_parameter("bike_deposit")
        elif service == "MOVE":
            param = get_parameter("move_deposit")
        else:
            return {'response': 'WRONG SERVICE'}

        if userFind["value"] >= param["value"]:
            return {'response': True}
        else:
            return {'response': False}
    return {'response': 'MISSING DATA RECEIVE'}


# Kiểm tra ví tiền của passenger có hợp lệ
@blue.route('/check_passenger_wallet', methods=['POST'])
@cross_origin()
def check_passenger_wallet():
    data = request.get_json()
    user_id = data.get('user_id')
    value = data.get('value')
    if user_id and value:
        userFind = get_wallet(user_id)
        if not userFind:
            return {'response': 'CANNOT FIND USER'}
        if userFind["value"] < value:
            return {'response': False}
        else:
            return {'response': True}
    return {'response': 'MISSING DATA RECEIVE'}

# Tính tiền trip ban đầu
@blue.route('/get_trip_price', methods=['POST'])
@cross_origin()
def get_price():
    data = request.get_json()
    if not data:
        return {"response": "NO DATA"}
    distance = data.get('distance')
    service = data.get('service')
    if not distance or not service:
        return {"response": "MISSING DATA"}
    param_data = None
    if service == "FOOD":
        param_data = get_parameter("food_fee")
    elif service == "SHOPPING":
        param_data = get_parameter("shopping_fee")
    elif service == "BIKE":
        param_data = get_parameter("bike_fee")
    elif service == "MOVE":
        param_data = get_parameter("move_fee")
    else:
        return {'response': 'SERVICE NOT VALID'}
    result = param_data["value"] * distance
    trip_money = calculate_rush_hour(result)
    return {'response': trip_money}

def calculate_rush_hour(trip_money):
    now = datetime.datetime.now()
    rush_hour_fee = get_parameter("rush_hour_fee")
    if now.hour >= 7 and now.hour <= 9:
        return (trip_money * rush_hour_fee['value']) / 100
    elif now.hour >= 17 and now.hour <= 19:
        return (trip_money * rush_hour_fee['value']) / 100
    return trip_money

# Tính tiền sau khi kết thúc chuyến đi
@blue.route('/end_trip', methods=['POST'])
@cross_origin()
def end_trip():
    data = request.get_json()
    if not data:
        return {"response": "NO DATA"}
    pass_id = data.get('passenger_id')
    driver_id = data.get('driver_id')
    trip_money = data.get('trip_money')
    discount_money = data.get('discount_money')
    service = data.get('service')
    payment_method = data.get('payment_method')
    discount_method = data.get('discount_method')
    order_id = data.get("order_id")

    if not order_id or not pass_id or not driver_id or not trip_money or not discount_money or not service or not payment_method or not discount_method:
        return {"response": "MISSING DATA"}
        
    pass_wallet = get_wallet(pass_id)
    pass_cash = get_cash(pass_id)
    driver_cash = get_cash(driver_id)
    driver_wallet = get_wallet(driver_id)
    core_wallet = get_wallet("core_wallet")

    if not pass_wallet or not pass_cash:
        return {"response": "NOT FOUND PASSENGER WALLET"}
    if not driver_wallet or not driver_cash:
        return {"response": "NOT FOUND DRIVER WALLET"}
    deposit = None
    if service == "FOOD":
        deposit = get_parameter("food_fee")
    elif service == "SHOPPING":
        deposit = get_parameter("shopping_fee")
    elif service == "BIKE":
        deposit = get_parameter("bike_fee")
    elif service == "MOVE":
        deposit = get_parameter("move_fee")
    else:
        return {'response': 'SERVICE NOT VALID'}
    
    if trip_money > discount_money:
        data_log = {
            "service" : service,
            "user_id" : pass_id,
            "value" : -(trip_money - discount_money),
            "order_id" : order_id
        }
        add_log_history(data_log)
    else:
        data_log = {
            "service" : service,
            "user_id" : pass_id,
            "value" : 0,
            "order_id" : order_id
        }
        add_log_history(data_log)

    if payment_method == "CASH":
        # UpdateWallet(pass_id,trip_money - discount_money)
        UpdateCash(driver_id, driver_cash['value'] + trip_money * (1 - (deposit["value"] / 100)))
        UpdateWallet(driver_id, driver_wallet['value'] - (trip_money * (deposit["value"] / 100)))
        if trip_money > discount_money:
            UpdateCash(pass_id, pass_cash['value'] - (trip_money * (1 - (deposit["value"] / 100))))

        UpdateWallet("core_wallet", core_wallet["value"] + (trip_money * (deposit["value"] / 100)))
    elif payment_method == "WALLET":
        UpdateCash(driver_id, driver_wallet['value'] + trip_money * (1 - (deposit["value"] / 100)))
        UpdateWallet(driver_id, driver_wallet['value'] - (trip_money * (deposit["value"] / 100)))
        if trip_money > discount_money:
            UpdateCash(pass_id, pass_wallet['value'] - (trip_money * (1 - (deposit["value"] / 100))))
        
        UpdateWallet("core_wallet", core_wallet["value"] + (trip_money * (deposit["value"] / 100)))
    else:
        return {'response': 'PAYMENT METHOD NOT VALID'}
    update_log_report(trip_money * (deposit["value"] / 100), "FEE")
    if discount_method == "REWARD":
        update_log_report(-discount_money, "REWARD")
    elif discount_method == "MEMBERSHIP":
        update_log_report(-discount_money, "MEMBERSHIP")
    return {'response': 'SUCCESS'}    

# Thêm tiền vào ví membership
@blue.route('/add_to_membership', methods=['POST'])
@cross_origin()
def add_to_membership():
    data = request.get_json()
    if not data:
        return {"response": "NO DATA"}
    cash = data.get('value')
    user_id = data.get('user_id')
    order_id = data.get('order_id')

    if not cash or not user_id or not order_id:
        return {"response": "MISSING DATA"}
    if cash <= 0:
        return {"response": "DATA INVALID"}

    membership_wallet = get_wallet("membership_wallet")
    UpdateWallet('membership_wallet', membership_wallet + cash)

    data_log = {
            "service" : "MEMBERSHIP",
            "user_id" : user_id,
            "value" : cash,
            "order_id" : order_id
        }
    add_log_history(data_log)
    update_log_report(cash, "MEMBERSHIP")
    return {"response": "SUCCESS"}
# Trừ tiền ví membership
@blue.route('/minus_to_membership', methods=['POST'])
@cross_origin()
def minus_to_membership():
    data = request.get_json()
    if not data:
        return {"response": "NO DATA"}
    cash = data.get('value')
    user_id = data.get('user_id')
    order_id = data.get('order_id')
    if not cash or not user_id or not order_id:
        return {"response": "MISSING DATA"}
    if cash <= 0:
        return {"response": "DATA INVALID"}
    membership_wallet = get_wallet("membership_wallet")
    UpdateWallet('membership_wallet', membership_wallet - cash)
    data_log = {
            "service" : "MEMBERSHIP",
            "user_id" : user_id,
            "value" : -cash,
            "order_id" : order_id
        }
    add_log_history(data_log)
    update_log_report(-cash, "MEMBERSHIP")
    return {"response": "SUCCESS"}
# Trừ tiền ví reward
@blue.route('/minus_to_reward', methods=['POST'])
@cross_origin()
def minus_to_reward():
    data = request.get_json()
    if not data:
        return {"response": "NO DATA"}
    cash = data.get('value')
    user_id = data.get('user_id')
    order_id = data.get('order_id')
    if not cash or not user_id or not order_id:
        return {"response": "MISSING DATA"}
    if cash <= 0:
        return {"response": "DATA INVALID"}
    reward_wallet = get_wallet("reward_wallet")
    UpdateWallet('reward_wallet', reward_wallet - cash)

    data_log = {
            "service" : "REWARD",
            "user_id" : user_id,
            "value" : -cash,
            "order_id" : order_id
        }
    add_log_history(data_log)
    update_log_report(cash, "REWARD")
    return {"response": "SUCCESS"}

@blue.route('/test_await', methods=['POST'])
@cross_origin()
def test_await():
    data = request.get_json()
    time.sleep(data.get('time'))
    return {"response": "SUCCESS"}

def add_log_history(data):
    if not data:
        return None
    data["date"] = datetime.datetime.now()
    db_log.insert_one(data)

@blue.route('/get_user_history', methods=['POST'])
@cross_origin()
def get_user_history():
    data = request.get_json()
    if not data:
        return {"response": "NO DATA"}
    user_id = data.get('user_id')
    service = data.get('service')

    if not service or not user_id:
        return {"response": "MISSING DATA"}
        
    log = db_log.find({}, {"_id": 0})
    list_log = []
    if list_log:
        for user_log in log:
            if user_log['user_id'] == user_id:
                list_log.append(user_log)
    return {"response": list_log}

@blue.route('/check_list_driver', methods=['POST'])
@cross_origin()
def check_list_driver():
    data = request.get_json()
    if not data:
        return {"response": "NO DATA"}
    list_driver = data.get('list_driver')
    price = data.get('value')
    service = data.get('service')
    if not list_driver or not price or not service:
        return {"response": "MISSING DATA"}
    valid_drivers = []
    for driver in list_driver:
        if check_valid_driver(driver, price, service) == True:
            valid_drivers.append(driver)
    return {"response": valid_drivers}


def check_valid_driver(driver_id, value, service):
    user_id = driver_id
    service = service
    if user_id and service:
        userFind = get_wallet(user_id)
        param = None
        charge = None
        if not userFind:
            return {'response': 'CANNOT FIND USER'}
        if service == "SHOPPING":
            param = get_parameter("shopping_deposit")
            charge = get_parameter("shopping_charge")
        elif service == "FOOD":
            param = get_parameter("food_deposit")
            charge = get_parameter("food_charge")
        elif service == "BIKE":
            param = get_parameter("bike_deposit")
            charge = get_parameter("bike_charge")
        elif service == "MOVE":
            param = get_parameter("move_deposit")
            charge = get_parameter("move_charge")
        else:
            return {'response': 'WRONG SERVICE'}

        if userFind["value"] - (value * (charge["value"] / 100)) >= param["value"]:
            return True
        else:
            return False
    return False

def update_log_report(value, service):
    dateNow = datetime.datetime.today().strftime('%d/%m/%Y')
    log = db_wallet.find_one({"date": dateNow}, {"_id": 0})
    if log:
        if service == "FEE":
            log["FEE"] = log["FEE"] + value
        elif service == "REWARD":
            log["REWARD"] = log["REWARD"] + value
        elif service == "MEMBERSHIP":
            log["MEMBERSHIP"] = log["MEMBERSHIP"] + value

        query = {'date': dateNow}
        value = {"$set": log}
        db_report.update_one(query, value)
    else:
        log["date"] = dateNow
        if service == "FEE":
            log["FEE"] = value
            log["REWARD"] = 0
            log["MEMBERSHIP"] = 0
        elif service == "REWARD":
            log["REWARD"] = value
            log["FEE"] = 0
            log["MEMBERSHIP"] = 0

        elif service == "MEMBERSHIP":
            log["REWARD"] = 0
            log["FEE"] = 0
            log["MEMBERSHIP"] = value
        
        db_report.insert_one(log)
        
# Get param
@blue.route('/get_user/<user_id>', methods=['GET'])
@cross_origin()
def get_user(user_id):
    if not user_id:
        return {'response': "MISSING USER ID"}

    cash = get_cash(user_id)
    wallet = get_wallet(user_id)
    if not cash or not wallet:
        return {'response': "CANNOT FIND USER CASH OR WALLET"}
    result = {}
    result["user_id"] = user_id
    result["cash"] = cash["value"]
    result["wallet"] = wallet["value"]
    return {'response': result}